%% Eval_Pop
% Evaluates a function for every individual in the population
%%
function pop = eval_pop_db_hypatia(pop,params,numberoftimesteps,numberofnodes,generation,Z)
v2struct(params); % unpack parameters

%%
nan=0; %NaN to check if cost is infeasable, if it is carbon will still be 0 and not NaN but we need to penalize that


projectpath = 'C:\Users\mobo\Desktop\cisbat\GA with ALL with penalty NEW\DG\'; % project directory
delete([projectpath '\result*']); % delete previous files
delete([projectpath '\QNode*']);
delete([projectpath '\Node*']);
[~,to_eval] = find([pop.evaluated]==0); % get indexes of solutions to evaluate
n_to_eval = size(to_eval,2);
fprintf('Running AIMMS (%g individuals).\n',n_to_eval); % report number of replacements

C = {'vec_iter := ', n_to_eval, ';'};
fileID = fopen(strcat(projectpath,'vec_iter.txt'),'w');
formatSpec = '%s %d %s';
fprintf(fileID,formatSpec,C{1,:});
fclose('all');


%% Delete previous 'Result.txt' file

% if exist(strcat(projectpath,'Result.txt'),'file')
%     delete(strcat(projectpath,'Result.txt'));
% end

%% Setting the capacities
% =======================

% Write Caps.txt for the energy converter capacities
% ==================================================

for i2=1:n_to_eval %this can be changed to which ever capacities are needed to be defined
    i = to_eval(i2);
    C = {
        'P_CHP_Max_ele(''1'') := ', pop(i).var(11), ';';...
        'P_CHP_Max_ele(''2'') := ', pop(i).var(12), ';';...
        'P_CHP_Max_ele(''3'') := ', pop(i).var(13), ';';...
        'P_CHP_Max_ele(''4'') := ', pop(i).var(14), ';';...
        'P_CHP_Max_ele(''5'') := ', pop(i).var(15), ';';...
        'H_Boiler_Max(''1'') := ', pop(i).var(1), ';';...
        'H_Boiler_Max(''2'') := ', pop(i).var(2), ';';...
        'H_Boiler_Max(''3'') := ', pop(i).var(3), ';';...
        'H_Boiler_Max(''4'') := ', pop(i).var(4), ';';...
        'H_Boiler_Max(''5'') := ', pop(i).var(5), ';';...
        'A_PV(''1'') := ', pop(i).var(6), ';';...
        'A_PV(''2'') := ', pop(i).var(7), ';';...
        'A_PV(''3'') := ', pop(i).var(8), ';';...
        'A_PV(''4'') := ', pop(i).var(9), ';';...
        'A_PV(''5'') := ', pop(i).var(10), ';';...
        'P_ASHP_max(''1'') := ',pop(i).var(16), ';';...
        'P_ASHP_max(''2'') := ',pop(i).var(17), ';';...
        'P_ASHP_max(''3'') := ',pop(i).var(18), ';';...
        'P_ASHP_max(''4'') := ',pop(i).var(19), ';';...
        'P_ASHP_max(''5'') := ',pop(i).var(20), ';';...
        'Storage_cap(''1'') := ',pop(i).var(21), ';';...
        'Storage_cap(''2'') := ',pop(i).var(22), ';';...
        'Storage_cap(''3'') := ',pop(i).var(23), ';';...
        'Storage_cap(''4'') := ',pop(i).var(24), ';';...
        'Storage_cap(''5'') := ',pop(i).var(25), ';';...
        };
    fileID = fopen(strcat(projectpath,'Caps_',num2str(i2),'.txt'),'w');
    formatSpec = '%s %d %s \r\n';
    
    [nrows,ncols] = size(C);
    for row = 1:nrows
        fprintf(fileID,formatSpec,C{row,:});
    end
    
    clear C;
    
end

%%
%%%%%%%%%%%%%%%%%%%%%
%%%%% CALL AIMMS %%%%
%%%%%%%%%%%%%%%%%%%%%


modelname = 'DG'; % model name of AIMMS (MILP)

[status.run,out] = system(['"C:\Program Files\AIMMS\AIMMS 4\Bin\aimms.exe" -m --run-only MainExecution "' projectpath '\' modelname '.aimms"']); %project path to AIMMS

if exist('soldb.mat','file') % if there is a DB file
    load soldb
else
    
    allsols_vars = int8.empty(0,nvar); % empty array for all vars
    allsols_data = {}; % cell for pop
    save('allsols_vars','allsols_data'); % save to DB
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%for each individual obtain objective functions%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i2 = 1:n_to_eval
    i = to_eval(i2);
    %%%%%%%%%%%%%%%
    %%obtain cost%%
    %%%%%%%%%%%%%%%
    
    if exist(strcat(projectpath,'Result_',num2str(i2),'.txt'),'file')
        fileID = fopen(strcat(projectpath,'Result_',num2str(i2),'.txt'));
        test=textscan(fileID,'%s %s','Delimiter','=');
        string=test{1,2}{1,1};
        number=strsplit(string,';');
        C=str2double(number{1,1});
        
        fclose(fileID);
        fclose('all');
        if isempty(C) || isnan(C)   %if infeasable, penalize
            pop(i).obj1 = 10^5; %cost
            pop(i).cons(3)=-10^10;
            nan=1;
        else
            pop(i).obj1 = C/10^6;
            for x=1:5
                fileID = fopen(strcat(projectpath,'MaxDif',num2str(x),'_',num2str(i2),'.txt'));
                test=textscan(fileID,'%s %s','Delimiter','=');
                string=test{1,2}{1,1};
                number=strsplit(string,';');
                P=str2double(number{1,1});
                fclose(fileID);
                fclose('all');
                if P>34
                    pop(i).cons(4)=pop(i).cons(4)-P;
                end
            end
        end
        
    else
        pop(i).obj1= 10^5; % Infeasibility check
        pop(i).cons(3)=-10^10;
        
        nan=1;
    end
    
    %%%%%%%%%%%%%%%%%%%
    %obtain emissions%%
    %%%%%%%%%%%%%%%%%%%
    
    if exist(strcat(projectpath,'Result2_',num2str(i2),'.txt'),'file')
        fileID = fopen(strcat(projectpath,'Result2_',num2str(i2),'.txt'));
        test=textscan(fileID,'%s %s','Delimiter','=');
        string=test{1,2}{1,1};
        number=strsplit(string,';');
        D=str2double(number{1,1});
        fclose(fileID);
        fclose('all');
        if isempty(D) || isnan(D) || (D==0 && nan==1) %emissions, if cost is infeasable nan=1
            pop(i).obj2 = 10^5; %if infeasable, penalize
            pop(i).cons(3)=-10^10;
        else
            pop(i).obj2 = D/10^6;
        end
        
    else
        pop(i).obj2= 10^5; % Infeasibility check
        pop(i).cons(3)=-10^10;
    end
    nan=0;
    %%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %############## MATPOWER #############
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %do powerflow calculation with matpower
     if exist(strcat(projectpath,'Result_',num2str(i2),'.txt'),'file') && exist(strcat(projectpath,'Result2_',num2str(i2),'.txt'),'file') && (isnan(C)==0) && (isnan(D)==0)
        for n=1:numberofnodes %number of buildings/nodes
            
            %%%%
            %%%% OBTAIN REAL POWER INJECTION/LOAD
            %%%%%
           if exist(strcat(projectpath,'Node',num2str(n),'_',num2str(i2),'.txt'),'file') %read REAL load/injections from txt file generated from AIMMS
                fileID = fopen(strcat(projectpath,'Node',num2str(n),'_',num2str(i2),'.txt'));
                N = textscan(fileID,'%s %s','Delimiter','='); %get injection at each node (P_CHP+P_PV etc)
                stringcell=N{1,2};
                stringcell=regexprep(stringcell, ' ;', '');
                Injection{1}=[stringcell];
                fclose(fileID);
                fclose('all');
                delete (strcat(projectpath,'Node',num2str(n),'_',num2str(i2),'.txt'));
            end
            Node{i2,n}=Injection{1}; %assign value of injection n to node i (building) in cell Node
            
            %%%%
            %%%% OBTAIN REACTIVE POWER LOAD
            %%%%%%
            if exist(strcat(projectpath,'QNode',num2str(n),'_',num2str(i2),'.txt'),'file') %read REACTIVE load/injections from txt file generated from AIMMS
                fileID = fopen(strcat(projectpath,'QNode',num2str(n),'_',num2str(i2),'.txt'));
                 N = textscan(fileID,'%s %s','Delimiter','='); %get injection at each node (P_CHP+P_PV etc)
                stringcell=N{1,2};
                stringcell=regexprep(stringcell, ' ;', '');
                QInjection{1}=[stringcell];
                fclose(fileID);
                fclose('all');
                delete (strcat(projectpath,'QNode',num2str(n),'_',num2str(i2),'.txt'));
            end
            QNode{i2,n}=QInjection{1}; %assign value of Q injection n to node i (building) in cell Node
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%
        %%%% CALL MATPOWER  %%%%
        %%%%%%%%%%%%%%%%%%%%%%%%
        
        define_constants;
        mpc=loadcase('CISBAT'); %load case from matpower
        mpopt=mpoption('verbose',0,'out.all',0); %do not print nice output
                
        for iter=1:numberoftimesteps %for each timestep do calculation
            for nodes=1:numberofnodes
                switch nodes % nodes is equal to building number in AIMMS but bus numbers are different in MATPOWER case
                    case 1
                        mpc.bus(2,PD)=str2double(Node{i2,nodes}(iter))/1000; %define REAL injection in the case for each node in MATPOWER in MW (/1000)
                        mpc.bus(2,QD)=str2double(QNode{i2,nodes}(iter))/1000; %define Reactive injection in the case for each node in MATPOWER in MVAr (/1000)
                    case 2
                        mpc.bus(4,PD)=str2double(Node{i2,nodes}(iter))/1000; %define REAL injection in the case for each node in MATPOWER in MW (/1000)
                        mpc.bus(4,QD)=str2double(QNode{i2,nodes}(iter))/1000; %define Reactive injection in the case for each node in MATPOWER in MVAr (/1000)
                    case 3
                        mpc.bus(5,PD)=str2double(Node{i2,nodes}(iter))/1000; %define REAL injection in the case for each node in MATPOWER in MW (/1000)
                        mpc.bus(5,QD)=str2double(QNode{i2,nodes}(iter))/1000; %define Reactive injection in the case for each node in MATPOWER in MVAr (/1000)
                    case 4
                        mpc.bus(6,PD)=str2double(Node{i2,nodes}(iter))/1000; %define REAL injection in the case for each node in MATPOWER in MW (/1000)
                        mpc.bus(6,QD)=str2double(QNode{i2,nodes}(iter))/1000; %define Reactive injection in the case for each node in MATPOWER in MVAr (/1000)
                    case 5
                        mpc.bus(7,PD)=str2double(Node{i2,nodes}(iter))/1000; %define REAL injection in the case for each node in MATPOWER in MW (/1000)
                        mpc.bus(7,QD)=str2double(QNode{i2,nodes}(iter))/1000; %define Reactive injection in the case for each node in MATPOWER in MVAr (/1000)
                end
            end
            
            results=runpf(mpc,mpopt); %run ACPF
            
            if iter==numberoftimesteps
                fprintf('MATPOWER was run for %i timesteps for individual %i.\n',iter,i); % report number of replacements
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%OBTAIN VOLTAGE RESULTS AND PENALIZE IF IT IS OUTSIDE +-5%##
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            Voltage=results.bus(:,VM);
            for k=1:size(Voltage,1)
                if Voltage(k)<0.90 || Voltage(k)>1.10
                    %pop(i).obj1=pop(i).obj1+abs(Voltage(k)-1)*10^5; % 10^6 is cost of penalize
                    %pop(i).obj2=pop(i).obj1+abs(Voltage(k)-1)*10^5; % 10^6 is cost of penalize
                    if Voltage(k)<0.90
                        pop(i).cons(1)=pop(i).cons(1)+(Voltage(k)-0.90)*1000;
                        pop(i).violated=1;
                    elseif Voltage(k)>1.10
                        pop(i).cons(1)=pop(i).cons(1)+(1.10-Voltage(k))*1000;
                        pop(i).violated=1;
                    end
                    pop(i).excessvoltage=pop(i).var; %save capacities which caused excess voltage
                    fileID=fopen('C:\Users\mobo\Desktop\cisbat\GA with ALL with penalty NEW\DG\ExcessVoltage.txt','a');
                    fprintf(fileID,'%s %i %s %i %s %i %s %f\r\n','Excess voltage for generation ',generation,' and population ',i2,' for bus ', k,' is ',Voltage(k)); %write the results
                    fclose(fileID);
                    fclose('all');
                end
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%CALCULATE LINE CURRENT%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            
            branchpowerflowTO=complex(results.branch(:,PT),results.branch(:,QT));
            branchpowerflowFROM=complex(results.branch(:,PF),results.branch(:,QF));
            Losses=(branchpowerflowTO+branchpowerflowFROM)*10^6; %from MW to W
            branchcurrent=sqrt(Losses./Z); %in Amp
            branchcurrent=abs(branchcurrent); %in Amp
            
            for branch=1:size(branchcurrent,1)
                if 250-abs(branchcurrent(branch))<0 %250 A is maximum current allowed (case dependent)
                    %pop(i).obj1=pop(i).obj1+abs(abs(branchcurrent(branch))-250)*10^3; % 10^3 is cost of penalize
                   % pop(i).obj2=pop(i).obj1+abs(abs(branchcurrent(branch))-250)*10^3; % 10^3 is cost of penalize
                    pop(i).cons(2)=pop(i).cons(2)+(250-abs(branchcurrent(branch)))/10;
                    pop(i).excesscurrent=pop(i).var; %save capacities which caused excess current
                    pop(i).violated=1;
                    fileID=fopen('C:\Users\mobo\Desktop\cisbat\GA with ALL with penalty NEW\DG\ExcessCurrent.txt','a');
                    fprintf(fileID,'%s %i %s %i %s %i %s %f\r\n','Excess current for generation ',generation,' and population ',i2,' for branch ', branch,' is ',branchcurrent(branch)); %write the results
                    fclose(fileID);
                    fclose('all');
                end
            end
            
            %             minVoltage{iter}=min(results.bus(:,VM)); %(min) voltage levels at each node/bus for each timestep
            %             maxPower{iter}=max(results.branch(:,PF)); %max real power for each timestep injected at each branch
            %             maxQPower{iter}=max(results.branch(:,QF)); %max real power for each timestep injected at each branch
            %             fileID=fopen('C:\Users\mobo\Box Sync\0PhD\!PhD\CISBAT 2015\GA with PV and CHP\DG\voltageAndPower.txt','a');
            %             fprintf(fileID,'%s %s %s %s %s %s %s\r\n','minimal voltage is: ',minVoltage{1,iter},' and max power is: ', maxPower{1,iter},' + ',maxQPower{1,iter},' j'); %write the results
            %             fclose(fileID);
            %             fclose('all');
        end
    end
    
    pop(i).con=sum(pop(i).cons(pop(i).cons<0)); % sum of all violated constraints

    pop(i).evaluated=1;
    allsols_vars(end+1,:) = pop(i).var;
    allsols_data{end+1} = pop(i); % add to DB
    
    save('allsols_vars','allsols_data');
    
    
end